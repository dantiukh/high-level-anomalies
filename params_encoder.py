import json

class ParamsEncoder(object):
    """A class for encoding hashable values as integers"""
    
    def __init__(self):
        self.encoded_dict = dict()
        
    def encode(self, value):
        """
        Encode a value. Encoding is guranteed to be in range
        [0, get_params_number(self)]. If a new value is encountered,
        get_params_number(self) will increase.
        Args:
          value: hashable, value to encode
        
        Returns:
          int: encoded value.
        """
        try:
            return self.encoded_dict[value]
        except KeyError:
            self.encoded_dict[value] = len(self.encoded_dict)
            return self.encoded_dict[value]

    def save_to_json(self, filename):
        """Saves self state to .json file"""
        with open(filename, 'w') as json_file:
            json.dump(self.encoded_dict, json_file)

    def load_from_json(self, filename):
        """Loads self state from .json"""
        with open(filename) as json_file:
            self.encoded_dict = json.load(json_file)
            
    def load_from_dict(self, dct):
        self.encoded_dict = dct
            
    def get_params_number(self):
        """Returnes the encoding range top. 
        Encoding is guranteed to be in range
        [0, get_params_number(self)]
        """
        return len(self.encoded_dict)

    def __len__(self):
        return self.get_params_number()

    def __getitem__(self, item):
        return self.encoded_dict[item]

    def get_decoder(self):
        return dict(map(reversed, self.encoded_dict.items()))
