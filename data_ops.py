import io
import os
import gzip
import ujson
import numpy as np

from scipy.sparse import coo_matrix, hstack, vstack
from operator import itemgetter
from config import DB_NAME, PRUNED_KEYS
from pymongo import MongoClient
from params_encoder import ParamsEncoder

def connect_to_mongo(port, host = 'localhost'):
    client = MongoClient(host, port, serverSelectionTimeoutMS = 5)
    try:
        dbn = client.database_names()
        print 'connected to database'
        return client
    except:
        print 'could not establish a connection to database'
        raise SystemError

def padded_vstack(blocks, format=None):
    width = max(map(lambda x: x.shape[1], blocks))
    reshaped_blocks = [
        hstack((block, coo_matrix((block.shape[0], width - block.shape[1])))) if
        block.shape[1] < width else block for block in blocks]
    return vstack(reshaped_blocks, format=format)
    
def merge_encoders(input_encoder, final_encoder):
    decoder = dict()
    for param, input_encoded in input_encoder.encoded_dict.items():
        decoder[final_encoder.encode(param)] = input_encoded
    return decoder, final_encoder

def build_encoding(col):
    keys = []
    for ob in col.find():
        keys += ob.keys()
    keys = list(set(keys))
    [keys.remove(t) for t in PRUNED_KEYS if t in keys]
    return {k:i for i,k in enumerate(sorted(keys))}

def load_run_from_db(cli, rnum, encoder=None):
    col_name = 'run_{}'.format(rnum)
    if col_name in cli[DB_NAME].collection_names():
        col = cli[DB_NAME][col_name]
        enc = build_encoding(col)
        return col_to_sparse(col, enc, final_encoder=None)
    else:
        print 'Collection {} not found in DB {}'.format(col_name, DB_NAME)
        
def encode_event(event, encoder):
    return {encoder.encode(k):event[k] for k in event if k not in PRUNED_KEYS}

def col_to_sparse(collection, input_encoding, final_encoder=None):
    input_encoder = ParamsEncoder()
    input_encoder.load_from_dict(input_encoding)
    if final_encoder:
        decoder, final_encoder = merge_encoders(input_encoder, final_encoder)
    else:
        decoder = None
        final_encoder = input_encoder
        
    values_list = []
    event_indices = []
    param_indices = []
    selected_events = 0

    for event in collection.find():
            event_dict = encode_event(event, final_encoder)
            for param, value in event_dict.items():
                if not value:
                    continue
                values_list.append(value)
                param_indices.append(param)
                event_indices.append(selected_events)
            selected_events += 1
    events = coo_matrix(
        (np.array(values_list, dtype=np.float), (
            np.array(event_indices, dtype=np.int),
            np.array(param_indices, dtype=np.int))),
        shape=(selected_events, len(final_encoder))).tocsc()

    if decoder:
        def decode_or_pass(value):
            try:
                return decoder[value]
            except KeyError:
                return value
        decoding_array = np.fromiter(map(
            decode_or_pass, range(len(final_encoder))), dtype=np.int, count=len(decoder))
        events = events[:, decoding_array]
        
    return events, final_encoder

