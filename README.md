### To perform mass processing of pairs of runs with XGBoost:
```
usage: mass_processing.py
```
This process can be configured through config.py
```
DB_NAME         Name of the database to read event data from
DB_HOST         Adress of the database host
DB_PORT         The port accepting MongoDB connections

RES_DB_NAME     Name of the database to write boosting results to
PRUNED_KEYS     Keys removed from the event data (e.g. eventNumber, runNumber)
XGB_PAR         XGBoost and cross-validation hyperparameters 
            (see http://xgboost.readthedocs.io/en/latest/python/python_api.html)
```
For an example, see compare_runs_xgb.ipynb

### To report results of gradient boosting:
```
usage:  print_xgb_report.py                 Prints AUC scores pair by pair 
        visualise_xgb.py                    Generates a bar chart of AUC scores
        notebooks/compare_runs_xgb.ipynb    Contains demo of the above
```
![](xgb_results.png)