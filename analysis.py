from data_ops import load_run_from_db, padded_vstack
from datetime import datetime

import xgboost as xgb
import numpy as np
import sklearn
import random
import gc

def compare_runs_xgb(pair, cli, sample_sizes, parameters):
    gc.collect()
    
    rounds = parameters['nrounds']
    folds = parameters['nfolds']
    bst_param = {k:parameters[k] for k in parameters if k not in ['nrounds', 'nfolds']}

    run_one, run_two = pair
    data_one, encoder = load_run_from_db(cli, run_one)
    data_two, encoder = load_run_from_db(cli, run_two, encoder)
    data = padded_vstack((data_one, data_two), format="csc")

    labels = np.concatenate((np.zeros(data_one.shape[0]),
                             np.ones(data_two.shape[0])))
    
    data, labels = sklearn.utils.shuffle(data, labels, random_state=34, replace=True)
    results = []
    for sample_size in sorted(sample_sizes):
        actual_sample_size = min(sample_size, data.shape[0])
        idx = np.random.choice(np.arange(labels.shape[0]), actual_sample_size, replace=False)
        dtrain = xgb.DMatrix(data[idx], label = labels[idx])
    
        bst = xgb.cv(bst_param, dtrain, num_boost_round=rounds, nfold=folds, metrics={'auc'}, seed = 0, as_pandas=False)
        R = {'parameters':parameters, 'results':bst.tolist(), 'sample_size':actual_sample_size,
	     'timestamp':datetime.now(), 'run_stats':{'run_one_num': str(run_one), 'run_one_size': data_one.shape[0],
						      'run_two_num': str(run_two), 'run_two_size': data_two.shape[0]}}     
        results.append(R)
        
    return results
