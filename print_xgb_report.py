from config import DB_HOST, DB_PORT, RES_DB_NAME
from data_ops import connect_to_mongo

cli = connect_to_mongo(DB_PORT, DB_HOST)
db = cli[RES_DB_NAME]
R = {}
for col in db.collection_names():
    R[col] = list(db[col].find())
    
col_names = ['test-auc-mean', 'test-auc-std', 'train-auc-mean', 'train-auc-std']

for run in R:
    for ss in R[run]:
        s1 = 'pair id: {}, sample size: {}'.format(run, ss['sample_size'])
	rd = ss['run_stats']
	s2 = 'run_{} size: {}, run_{} size: {}'.format(rd['run_one_num'], rd['run_one_size'],	
						       rd['run_two_num'], rd['run_two_size'])
        ml = max(len(s1), len(s2))
	print ml*'='
        print s1
	print s2

        for col, d in zip(col_names, ss['results'][-1]):
            print '{0: <15}: {1: <15}'.format(col, d)

print ml*'='
print 'Boosting parameters:'
for k in ss['parameters']:
    print '{0: <10}: {1: <20}'.format(k, ss['parameters'][k])
print ml*'=' + '\n'
