DB_NAME = 'lhcb'
DB_HOST = '10.128.124.12'
DB_PORT = 27017
RES_DB_NAME = 'xgb_t2'

PRUNED_KEYS = ['_id', 'eventNumber', 'gpsTime', 'runNumber']
XGB_PAR = {'max_depth':5, 'eta':1, 'silent':True, 'objective':'binary:logistic',
           'nthreads':2, 'nrounds':10, 'nfolds':5}
