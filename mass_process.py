import json
import requests
import numpy as np

from config import DB_PORT, DB_HOST, DB_NAME, RES_DB_NAME, XGB_PAR
from analysis import compare_runs_xgb
from operator import itemgetter
from collections import defaultdict
from itertools import combinations, chain
from data_ops import connect_to_mongo

def get_run_pairs(cli):
    rnames = []
    for c in cli[DB_NAME].collection_names():
        try:
            rname = int(c.split('_')[1])
            rnames.append(rname)
        except Exception as e:
            print e
    return runs_to_pairs(rnames)

def get_fill_id(run_id):
    rundb_url = "http://lbrundb.cern.ch/api/run/%d"
    return json.loads(requests.get(rundb_url % run_id).text)["fillid"]

def runs_to_pairs(runs):
    fills_by_run = dict(map(lambda run: (run, get_fill_id(run)), runs))
    print 'fills retrieved...'
    runs_by_fill = defaultdict(list)
    for run_id, fill_id in fills_by_run.items():
        runs_by_fill[fill_id].append(run_id)
    runs_in_fills = list(map(itemgetter(0, 1), filter(lambda x: len(x) >= 2, runs_by_fill.values())))
    runs_between_fills = list(map(lambda fills_pair: (
            runs_by_fill[fills_pair[0]][0],
            runs_by_fill[fills_pair[1]][0]),
                             combinations(runs_by_fill.keys(), 2)))
    return list(chain(runs_in_fills, runs_between_fills))

def mass_process():
    cli = connect_to_mongo(DB_PORT, DB_HOST)

    pairs = get_run_pairs(cli)
    sizes = (20000, 50000, 100000)

    done = cli[RES_DB_NAME].collection_names()
    print '{} pairs already processed'.format(len(done))
    TBD = []

    for pair in pairs:
        pnam = str(pair[0]) + '_' + str(pair[1])
        if pnam not in done:
            TBD.append((pair, pnam))

    if not len(TBD):
        print 'Nothing to do, exiting.'
        return 0
	
    #print TBD

    print '{} pairs to process'.format(len(TBD))
    print 'Boosting with the following parameters: {}'.format(XGB_PAR)
    for i, p in enumerate(TBD):
        try:
	    pair, col = p
            print 'Boosting {}/{}, pair ID: {}'.format(i+1, len(TBD), col)

            r = compare_runs_xgb(pair, cli, sizes, XGB_PAR)
            cli[RES_DB_NAME][col].insert(r, check_keys=False)
        except Exception as e:
            print e
            
if __name__ == "__main__":
    mass_process()
